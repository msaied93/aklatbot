@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>

        <div class="panel-body">
            <h3>Bot Info (getMe):</h3>
            <p>
            @foreach ($botInfo as $info => $value)
            {{ $info }}: {{ $value }}<br>
            @endforeach
            </p>

            <h3>Webhooks Info (getWebhookInfo):</h3>
            <p>
            @foreach ($webhookInfo as $info => $value)
            {{ $info }}: {{ $value }}<br>
            @endforeach
            </p>
        </div>
    </div>
</div>
@endsection
