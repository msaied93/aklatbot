<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Telegram;

class AdminBotController extends Controller
{
    public function index()
    {
        $botInfo = json_decode(Telegram::getMe(), TRUE);

        $webhookInfo = [];

        return view('admin.index', compact('botInfo', 'webhookInfo'));
    }

    public function setWebhook()
    {
    	return Telegram::setWebhook(['url' => url('/api/telegram/aklatbot/webhook')]);
    }

    public function deleteWebhook()
    {
    	return Telegram::removeWebhook();
    }
}
