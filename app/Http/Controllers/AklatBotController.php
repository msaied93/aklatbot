<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Telegram;

class AklatBotController extends Controller
{
    public function hookHandler(Request $request)
    {
        // \Log::warning(Telegram::getWebhookUpdates()['message']['chat']['id']);
        $chatId = $request->message['chat']['id'];
        $text = $request->message['text'];
        if( !(empty($chatId) || empty($text)))
        Telegram::sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
        ]);

        // return response()->json();
    }
}
